<?php

$res = $sth->execute();
if ($res)
{
    $items = $sth->fetchAll(PDO::FETCH_ASSOC);
    $liste = [
        "items" => $items
    ];
    
    $json = json_encode($liste);
    
    header("Access-Control-Allow-Origin: http://localhost:3000");
    echo $json;
}
else 
{
    header("Internal error", true, 500);
    //echo $sth->errorInfo()[2];
}
