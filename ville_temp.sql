-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 28 juin 2019 à 06:38
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ville_temp`
--

-- --------------------------------------------------------

--
-- Structure de la table `temperature`
--

DROP TABLE IF EXISTS `temperature`;
CREATE TABLE IF NOT EXISTS `temperature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moment` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valeur` float NOT NULL,
  `id_ville` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_ville` (`id_ville`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `temperature`
--

INSERT INTO `temperature` (`id`, `moment`, `valeur`, `id_ville`) VALUES
(1, '2019-06-25 13:50:19', 31, 1),
(2, '2019-06-25 13:50:19', 31.5, 2),
(3, '2019-06-25 13:50:19', 29, 3),
(4, '2019-06-25 13:51:15', 32, 1),
(5, '2019-06-25 13:51:15', 30.5, 2),
(6, '2019-06-25 13:51:15', 30.5, 3),
(7, '2019-06-25 13:52:39', 33, 1),
(8, '2019-06-25 13:52:39', 30.5, 2),
(9, '2019-06-25 13:52:39', 31, 3),
(10, '2019-06-25 14:41:52', 35, 1),
(11, '2019-06-25 14:41:52', 29, 2),
(12, '2019-06-25 14:41:52', 25, 3),
(13, '2019-06-25 14:57:34', 40, 1),
(14, '2019-06-25 15:04:26', 39, 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `temp_actuelles`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `temp_actuelles`;
CREATE TABLE IF NOT EXISTS `temp_actuelles` (
`id` int(11)
,`nom` varchar(50)
,`temperature` float
);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id`, `nom`) VALUES
(1, 'Valenciennes'),
(2, 'Aulnoy-lez-Valenciennes'),
(3, 'Famars');

-- --------------------------------------------------------

--
-- Structure de la vue `temp_actuelles`
--
DROP TABLE IF EXISTS `temp_actuelles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `temp_actuelles`  AS  select `ville`.`id` AS `id`,`ville`.`nom` AS `nom`,`temperature`.`valeur` AS `temperature` from (`ville` join `temperature` on((`temperature`.`id_ville` = `ville`.`id`))) where (`temperature`.`id` = (select max(`temperature`.`id`) from `temperature` where (`temperature`.`id_ville` = `ville`.`id`))) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
